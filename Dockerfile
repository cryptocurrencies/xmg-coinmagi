# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libgmp-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/magi-project/magi.git /opt/coinmagi
RUN cd /opt/coinmagi/src && \
	make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
	apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r coinmagi && useradd -r -m -g coinmagi coinmagi
RUN mkdir /data
RUN chown coinmagi:coinmagi /data
COPY --from=build /opt/coinmagi/src/magid /usr/local/bin/
USER coinmagi
VOLUME /data
EXPOSE 8232 8233
CMD ["/usr/local/bin/magid", "-datadir=/data", "-conf=/data/magi.conf", "-server", "-txindex", "-printtoconsole"]